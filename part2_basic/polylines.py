# coding = utf-8
import numpy as np
import cv2

# Create a black image
img = np.ones((512, 512, 3), np.uint8)

pts = np.array([[20, 50], [10, 330], [350, 410], [470, 220], [270, 30]], np.int32)
pts = pts.reshape((-1, 1, 2))
# 这里 reshape 的第一个参数为-1, 表明这一维的长度是根据后面的维度的计算出来的。

cv2.polylines(img, [pts], True, (255, 255, 255), 5)
cv2.imshow("1", img)
cv2.waitKey()