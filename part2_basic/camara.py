# -*- coding:utf-8-*-

import numpy as np
import cv2

isOpen = False


def init(id):
    global cap
    cap = cv2.VideoCapture(id)
    fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
    size = (int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH)),
            int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT)))
    print fps, size
    global isOpen
    isOpen = True


def capture(id):
    global cap, isOpen
    if isOpen:
        ret, frame = cap.read()
        # if ret == True:
        # cap.release()
        return frame
    else:
        init(id)
        capture(id)


def close():
    global cap
    cap.release()


if __name__ == '__main__':
    while True:
        img = capture(1)
        if img is None:
            cv2.waitKey(500)
            continue
        cv2.imshow('frame', img)
        cv2.waitKey(10)
        if 0xFF == ord('q') & cv2.waitKey(10) == 27:
            cv2.destroyAllWindows()
            break
