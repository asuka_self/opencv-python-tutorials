# -*- coding: utf-8 -*-
import cv2
import numpy as np
img = cv2.imread('./part3_main_operation/1.png')

# 通道拆分
b, g, r = cv2.split(img)
cv2.imshow("b", b)
cv2.imshow("g", g)
cv2.imshow("r", r)

# 绿色通道整体赋值
img[:, :, 1] = 0

# 或者，提取绿色通道
r1 = img[:, :, 1]
cv2.imshow("r1", r1)

# 融合通道
img = cv2.merge([b, g, r1])
cv2.imshow("out", img)

cv2.waitKey(0)
