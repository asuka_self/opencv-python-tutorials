# -*- coding: utf-8 -*-
import cv2
import numpy as np
img1 = cv2.imread('./part3_main_operation/1.png')
img2 = cv2.imread('./part3_main_operation/2.jpg')
cv2.imshow("img1", img1)
cv2.imshow("img2", img2)

# numpy加法
img3 = img1 + img2
# opencv加法，推荐
img4 = cv2.add(img1, img2)
# 图像混合，权重
img5 = cv2.addWeighted(img1, 0.7, img2, 0.3, 0)

cv2.imshow("img3", img3)
cv2.imshow("img4", img4)
cv2.imshow("img5", img5)
'''
我想把 OpenCV 的标志放到另一幅图像上。如果我使用加法，颜色会改
变，如果使用混合，会得到透明效果，但是我不想要透明。如果他是矩形我可
以象上一章那样使用 ROI。但是他不是矩形。但是我们可以通过下面的按位运
算实现：
'''
# I want to put logo on top-left corner, So I create a ROI
rows, cols, channels = img2.shape
roi = img1[0:rows, 0:cols]
# Now create a mask of logo and create its inverse mask also
img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
ret, mask = cv2.threshold(img2gray, 120, 255, cv2.THRESH_BINARY)
# cv2.imshow('mask', mask)
mask_inv = cv2.bitwise_not(mask)
# cv2.imshow('mask_inv', mask_inv)
# Now black-out the area of logo in ROI
# 取 roi 中与 mask 中不为零的值对应的像素的值，其他值为 0
# 注意这里必须有 mask=mask 或者 mask=mask_inv, 其中的 mask= 不能忽略
img1_bg = cv2.bitwise_and(roi, roi, mask=mask)
# 取 roi 中与 mask_inv 中不为零的值对应的像素的值，其他值为 0。
# Take only region of logo from logo image.
img2_fg = cv2.bitwise_and(img2, img2, mask=mask_inv)
# Put logo in ROI and modify the main image
img6 = cv2.add(img1_bg, img2_fg)
cv2.imshow('img6', img6)

cv2.waitKey(0)
