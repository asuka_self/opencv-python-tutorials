# -*- coding: utf-8 -*-
'''
Created on Sat Jan 11 09:32:51 2014
@author: duan

在缩放时我们推荐使用 cv2.INTER_AREA，
在扩展时我们推荐使用 v2.INTER_CUBIC（慢) 和 v2.INTER_LINEAR。
默认情况下所有改变图像尺寸大小的操作使用的插值方法都是 cv2.INTER_LINEAR。
'''

import cv2
import numpy as np
img = cv2.imread('./part3_main_operation/1.png')
# 下面的 None 本应该是输出图像的尺寸，但是因为后边我们设置了缩放因子
# 因此这里为 None
res1 = cv2.resize(img, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)
#OR
# 这里呢，我们直接设置输出图像的尺寸，所以不用设置缩放因子
height, width = img.shape[:2]
res2 = cv2.resize(img, (width/2, height/2), interpolation=cv2.INTER_AREA)
while (1):
    cv2.imshow('img', img)
    cv2.imshow('res1', res1)
    cv2.imshow('res2', res2)
    if cv2.waitKey(1) & 0xFF == 27:
        break
cv2.destroyAllWindows()