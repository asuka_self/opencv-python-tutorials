# -*- coding: utf-8 -*-
"""
Created on Fri Jan 10 20:25:00 2014
@author: duan
"""
import cv2
import numpy as np
from matplotlib import pyplot as plt

cap = cv2.VideoCapture(0)

while (1):
    # 获取每一帧
    ret, img = cap.read()

    # 通道拆分
    b, g, r = cv2.split(img)

    # # 显示直方图
    # # #flatten() 将数组变成一维
    # hist, bins = np.histogram(gray.flatten(), 256, [0, 256])
    # # 计算累积分布图
    # cdf = hist.cumsum()
    # cdf_normalized = cdf * hist.max() / cdf.max()
    # plt.plot(cdf_normalized, color='b')
    # plt.hist(gray.flatten(), 256, [0, 256], color='r')
    # plt.xlim([0, 256])
    # plt.legend(('cdf', 'histogram'), loc='upper left')
    # plt.show()

    # 直方图均衡化
    b_equ = cv2.equalizeHist(b)
    g_equ = cv2.equalizeHist(g)
    r_equ = cv2.equalizeHist(r)
    # 双图显示
    b_res = np.hstack((b, b_equ))
    g_res = np.hstack((g, g_equ))
    r_res = np.hstack((r, r_equ))

    # 融合通道
    dst = cv2.merge([b_equ, g_equ, r_equ])

    dst_res = np.hstack((img, dst))

    # 显示图像
    cv2.imshow('b_res', b_res)
    cv2.imshow('g_res', g_res)
    cv2.imshow('r_res', r_res)
    cv2.imshow('dst_res', dst_res)

    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

# 关闭窗口
cv2.destroyAllWindows()