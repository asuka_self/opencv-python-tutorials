# -*- coding: utf-8 -*-
"""
Created on Sat Jan 11 10:06:28 2014
@author: duan

对一个图像旋转角度 θ, 需要使用到下面形式的旋转矩阵。
M = 2 4 cos sin θ θ − cos sinθθ 3 5
但是 OpenCV 允许你在任意地方进行旋转，但是旋转矩阵的形式应该修
改为
24
α β (1 − α) · center:x − β · center:y
−β α β · center:x + (1 − α) · center:x
35
其中：
α = scale · cos θ
β = scale · sin θ
为了构建这个旋转矩阵， OpenCV 提供了一个函数：cv2.getRotationMatrix2D。
"""
import cv2
import numpy as np
img = cv2.imread('./part3_main_operation/1.png')
rows, cols, depth = img.shape
# 这里的第一个参数为旋转中心，第二个为旋转角度，第三个为旋转后的缩放因子
# 可以通过设置旋转中心，缩放因子，以及窗口大小来防止旋转后超出边界的问题
M = cv2.getRotationMatrix2D((cols / 2, rows / 2), -45, 0.8)
# 第三个参数是输出图像的尺寸中心
dst = cv2.warpAffine(img, M, (2 * cols, 2 * rows))
while (1):
    cv2.imshow('img', dst)
    if cv2.waitKey(1) & 0xFF == 27:
        break
cv2.destroyAllWindows()