# -*- coding: utf-8 -*-
"""
Created on Fri Jan 10 20:25:00 2014
@author: duan
"""
import cv2
import numpy as np
from matplotlib import pyplot as plt

cap = cv2.VideoCapture(0)

while (1):
    # 获取每一帧
    ret, img = cap.read()
    # 转换到 GRAY
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # # 显示直方图
    # # #flatten() 将数组变成一维
    # hist, bins = np.histogram(gray.flatten(), 256, [0, 256])
    # # 计算累积分布图
    # cdf = hist.cumsum()
    # cdf_normalized = cdf * hist.max() / cdf.max()
    # plt.plot(cdf_normalized, color='b')
    # plt.hist(gray.flatten(), 256, [0, 256], color='r')
    # plt.xlim([0, 256])
    # plt.legend(('cdf', 'histogram'), loc='upper left')
    # plt.show()

    # 直方图均衡化
    equ = cv2.equalizeHist(gray)
    # 双图显示
    res = np.hstack((gray, equ))

    # 显示图像
    cv2.imshow('img', img)
    cv2.imshow('res', res)
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

# 关闭窗口
cv2.destroyAllWindows()