# -*- coding: utf-8 -*-
"""
Created on Sat Jan 11 10:51:19 2014
@author: duan
对于视角变换，我们需要一个 3x3 变换矩阵。在变换前后直线还是直线。
要构建这个变换矩阵，你需要在输入图像上找 4 个点，以及他们在输出图
像上对应的位置。这四个点中的任意三个都不能共线。这个变换矩阵可以有
函数 cv2.getPerspectiveTransform() 构建。然后把这个矩阵传给函数
cv2.warpPerspective。
"""
import cv2
import numpy as np
from matplotlib import pyplot as plt
img = cv2.imread('./part4_imageprocess/perspective2.png')
rows, cols, ch = img.shape
colSpan = 4
rowSpan = 4
pts1 = np.float32([[382, 149], [425, 149], 
                   [cols / 2 - 175, rows], [cols / 2 + 175, rows]])
pts2 = np.float32([[cols * colSpan / 2 - 175, 149 * 2], [cols * colSpan / 2 + 175, 149 * 2],
                   [cols * colSpan / 2 - 175, rows * rowSpan], [cols * colSpan / 2 + 175, rows * rowSpan]])
M = cv2.getPerspectiveTransform(pts1, pts2)
dst = cv2.warpPerspective(img, M, (cols * colSpan, rows * rowSpan))

# plt.subplot(121, plt.imshow(img), plt.title('Input'))
# plt.subplot(121, plt.imshow(img), plt.title('Output'))
# plt.show()
cv2.imwrite('./part4_imageprocess/perspective2_out.png', dst)
# cv2.imshow("dst", dst)
# cv2.waitKey(0)